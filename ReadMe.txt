This program takes in a csv file and prints ( on the command line interface) the sorted table.

Following is an example of how to use the jar file.

1) Download the CodingAssignment.jar file from the root directory.
2) Download one of the sample csv file located into the test folder.
3) Setup the following command place it into the CLI and press enter.
    java -jar <location of the CodingAssignment.jar file> <location of the csv file to be sorted>

Following is an example of running  this jar file to sort one of the test csv files (Import_User_Sample_en.csv).

jar file = C:\Users\user1\Desktop\CodingAssignment.jar
csv file = C:\Users\user1\Downloads\csv\Import_User_Sample_en.csv

I am directing the output to a file so i can inspect later with ease.


Unsorted file example.

User Name,First Name,Last Name,Display Name,Job Title,Department,Office Number,Office Phone,Mobile Phone,Fax,Address,City,State or Province,ZIP or Postal Code,Country or Region
chris@contoso.com,Chris,Green,Chris Green,IT Manager,Information Technology,123451,123-555-1211,123-555-6641,123-555-9821,1 Microsoft way,Redmond,Wa,98052,United States
ben@contoso.com,Ben,Andrews,Ben Andrews,IT Manager,Information Technology,123452,123-555-1212,123-555-6642,123-555-9822,1 Microsoft way,Redmond,Wa,98052,United States
david@contoso.com,David,Longmuir,David Longmuir,IT Manager,Information Technology,123453,123-555-1213,123-555-6643,123-555-9823,1 Microsoft way,Redmond,Wa,98052,United States
cynthia@contoso.com,Cynthia,Carey,Cynthia Carey,IT Manager,Information Technology,123454,123-555-1214,123-555-6644,123-555-9824,1 Microsoft way,Redmond,Wa,98052,United States
melissa@contoso.com,Melissa,MacBeth,Melissa MacBeth,IT Manager,Information Technology,123455,123-555-1215,123-555-6645,123-555-9825,1 Microsoft way,Redmond,Wa,98052,United States 




command to paste into the Windows CLI is the following 
java -jar C:\Users\user1\Desktop\CodingAssignment.jar  C:\Users\user1\Downloads\csv\Import_User_Sample_en.csv   >  C:\Users\user1\Downloads\csv\Import_User_Sample_en_sorted.log     2>&1

The command above generates the following log and the resulting sorted csv dataset.

*******************************
This program is for sorting a comma delimited file.
It will use the first column to sort
The column will be sorted alphanumerically
The resulting file will be printed to the screen 
*******************************

[04-08-16 23:10:07]: Following CSV file name was provided.
[04-08-16 23:10:07]: C:\Users\user1\Downloads\csv\Import_User_Sample_en.csv
[04-08-16 23:10:07]: Confirmed the file exists.
[04-08-16 23:10:07]: Found 6 lines, including header.
[04-08-16 23:10:07]: Header contains 15 columns. 
[04-08-16 23:10:07]: Valid CSV file. Parsed properly. It is not malformed.
[04-08-16 23:10:07]: Identified unique value for this column. Found 5 unique values.
[04-08-16 23:10:07]: Started printing sorted table 

User Name,First Name,Last Name,Display Name,Job Title,Department,Office Number,Office Phone,Mobile Phone,Fax,Address,City,State or Province,ZIP or Postal Code,Country or Region
ben@contoso.com,Ben,Andrews,Ben Andrews,IT Manager,Information Technology,123452,123-555-1212,123-555-6642,123-555-9822,1 Microsoft way,Redmond,Wa,98052,United States
chris@contoso.com,Chris,Green,Chris Green,IT Manager,Information Technology,123451,123-555-1211,123-555-6641,123-555-9821,1 Microsoft way,Redmond,Wa,98052,United States
cynthia@contoso.com,Cynthia,Carey,Cynthia Carey,IT Manager,Information Technology,123454,123-555-1214,123-555-6644,123-555-9824,1 Microsoft way,Redmond,Wa,98052,United States
david@contoso.com,David,Longmuir,David Longmuir,IT Manager,Information Technology,123453,123-555-1213,123-555-6643,123-555-9823,1 Microsoft way,Redmond,Wa,98052,United States
melissa@contoso.com,Melissa,MacBeth,Melissa MacBeth,IT Manager,Information Technology,123455,123-555-1215,123-555-6645,123-555-9825,1 Microsoft way,Redmond,Wa,98052,United States
[04-08-16 23:10:07]: Completed printing sorted table 


the file was sorted alphanumerically by the value in the first column.


if you have any questions or concern please contact sami at mail.mhaque@gmail.com