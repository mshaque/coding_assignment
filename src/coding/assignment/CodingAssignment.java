package coding.assignment;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CodingAssignment {    

	
	// entry point for the class.
	public static void main(String[] args) {		
		
		// printing the assignment details
		create_assignment_str();
		
		// providing the column number used for the sorting action.
		// this variable can easily be configurable from a file or 
		// taken in as a CLI input addition to the file address.
		int sort_col = 0;
		
		// collecting the file address from CLI arguments.
		String fileAddresss = validate_file_existance(args);
		ArrayList<String[]> content = null;
		
		// Loading the file as an array list of an array of string.
		if (fileAddresss != null){			
			try {
				content = load_file_content_to_memory(fileAddresss);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}else {
			simple_logger("Failed to process provided file. Exiting program.");
		}
		
		// Processing the collected data. 
		ArrayList<String>  uniqueList = new ArrayList<String>();
		if (content != null) {
			// Validating CSV content.
			// counts the line number and column number
			// iterates through the whole file and compares 
			// the column count for each line.
			// detects melformed CSV.
			validate_csv_content(content);
			
			// collects the unique list data records that is to be sorted.
			uniqueList = collect_unique_column_value(content, sort_col);
			
			// sorting the list of data-records using generic sorting function.
			Collections.sort(uniqueList);
			
			//organizing the file based on the sorted list generated above
			ArrayList<String[]> sorted_content = generate_sorted_arraylist(content,uniqueList,sort_col);
			
			// printing the sorted list.
			print_sorted_table(sorted_content);
		}
	}
	
	
	// This function prints the sorted table.
	// This takes in an arrayList of array of strings.
	public static void print_sorted_table(ArrayList<String[]> content){
		simple_logger("Started printing sorted table \n");
		// iterating through the sorted list of table.
		for (int i = 0; i < content.size(); i++){
			// printing each of the sorted record by joining then with ',' delimiter. 
			System.out.println(String.join(",", content.get(i)));
		}
		simple_logger("Completed printing sorted table \n");
	}
	
	
	// this function uses the sorted unique data records 
	// to sort rest of the table.
	public static ArrayList<String[]> generate_sorted_arraylist(ArrayList<String[]> content, ArrayList<String> uniqueList, int col_number){		
		ArrayList<String[]> sorted_table = new ArrayList<String[]>();
		Map<String, ArrayList<Integer>> uniqueListMap = new HashMap<String, ArrayList<Integer>>();
		
		// creating a hashmap using the unique data-record as key.
		// the value is a list integer. 
		// each integer is an index from the master table that contains this unique key.
		ArrayList<Integer>  tmpList = new ArrayList<Integer>();
		for (String value_name: uniqueList){
			uniqueListMap.put(value_name, (ArrayList) tmpList.clone());
		}
		
		// iterating through each record
		// generating a complete list of indexes that contain this 
		// particular record.
		for (int i = 1; i < content.size(); i++){
			uniqueListMap.get(String.valueOf(content.get(i)[col_number])).add(i);
		}
		
		// loading the header into the sorted table list.		
		sorted_table.add(content.get(0));
		
		// iterating through each unique record and their manifest or list of indexes 
		// that contains this particular record in the master table.
		// loading that record into the sorted table.
		for (String value_name: uniqueList){			
			for (int index_num: uniqueListMap.get(value_name)){				
				sorted_table.add(content.get(index_num));
			}			
		}
		
		// returning the sorted table to the main method.
		return sorted_table;
	}
	
	
	// collecting the unique list of records from the column that require sorting.
	public static ArrayList<String> collect_unique_column_value(ArrayList<String[]> content, int col_number){		
		ArrayList<String> unique_value = new ArrayList<String>();		
		//iterating through the list while excluding the header.
		for (int i = 1; i < content.size(); i++){
			// if the value is not in the unique list.
			// we add it to the unique list.
			if (!unique_value.contains(String.valueOf(content.get(i)[col_number]))){
				unique_value.add(String.valueOf(content.get(i)[col_number]));
			}
		}
		simple_logger("Identified unique value for this column. Found " + String.valueOf(unique_value.size()) + " unique values.") ; 
		return unique_value;
	}
	
	// this function validates the csv content.
	public static void validate_csv_content(ArrayList<String[]> content){
		// prints the total row count.
		simple_logger("Found " + String.valueOf(content.size()) + " lines, including header." );	
		
		// counting the header. and printing it.
		int header_column = content.get(0).length ;		
		simple_logger("Header contains " + String.valueOf(header_column) + " columns. " );		
		
		
		// iterating through the table and counting the 
		// checking column count for each record against the header.
		// printing if a malformed record is found.		
		String result_val = "Valid CSV file. Parsed properly. It is not malformed.";
		for ( String[] line:content ){
			if (line.length != header_column ) {
				result_val = "Malformed csv file. record contain unexpected number of columns." + String.valueOf(line.length) ; 
				
			}	
		}
		simple_logger(result_val);	
	}
	
	// loads the file content into memory.
	public static ArrayList<String[]> load_file_content_to_memory(String fileAddresss) throws IOException {		
		ArrayList<String[]> list = new ArrayList<String[]>();
		String line = "";
		BufferedReader br = new BufferedReader(new FileReader(fileAddresss));
		
		// separating the content using line break.
		while ((line = br.readLine()) != null) {
			
			// separating each line using ',' ( commma) as delimiter.
			list.add(line.split(","));
		}
		
		// returning a ArrayList of array of list.
		return list;		
	}

	

	
	public static void simple_logger(String data){
		String timeStamp = new SimpleDateFormat("[MM-dd-yy HH:mm:ss]: ").format(Calendar.getInstance().getTime());
		timeStamp += data;
		System.out.println(timeStamp);
		
	}
	
	
	// checks if the file exists.
	public static String validate_file_existance(String[] args){
		String filePathString = null;
		
		// if a file address is not provided, following message is dispated.
		if (args.length < 1) {
			simple_logger("Use:");
			simple_logger("\tjava -jar CodingAssignment.jar <csv file address.>");
			return null;
		}else{
			filePathString = String.valueOf(args[0]);
			simple_logger("Following CSV file name was provided.");
			simple_logger(filePathString);
		}
		
		File f = new File(filePathString);
		if(f.exists() && !f.isDirectory()) { 
			simple_logger("Confirmed the file exisits.");
		}else{
			simple_logger("Failed to confirm file's existance. Cannot continue");
		}
		return filePathString;
	}
	
	// this function prints the assignment provided.	
	public static void create_assignment_str(){
		String assignment_str = "\n*******************************\n";
		assignment_str += "This program is for sorting a comma delimited file.\n";
		assignment_str += "It will use the first column to sort\n";
		assignment_str += "The column will be sorted alphanumerically\n";
		assignment_str += "The resulting file will be printed to the screen \n";
		assignment_str += "*******************************\n";
		System.out.println(assignment_str);	
	}	
}
